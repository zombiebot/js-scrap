var WorkTime = {
    "totalTime" : 1500,     //time in seconds
    "leftTime" : 1500,      //should be equal to totalTime
    "repeater" : 1,       //time in seconds for recalculating time
}

function updateLeftTime() {
    WorkTime.leftTime -= WorkTime.repeater;
}

function getTimeInMinutes() {
    return Math.ceil(WorkTime.leftTime / 60);
}

function resetTime() {
    WorkTime.leftTime = WorkTime.totalTime;
}

function getTimeLeftPecentPart() {
    var timePassed = WorkTime.totalTime - WorkTime.leftTime
    return timePassed / WorkTime.totalTime
}
