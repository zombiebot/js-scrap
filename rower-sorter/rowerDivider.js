// Script to divene rowers 

var rd = {};

rd.members = [];
rd.boats = [];
rd.boat_seats = 8;
rd.unluckyMembers = []; //array to save members which was not selected for boat (debug purpose)

/** Load members data. Please refer to data.js for data stucture reference */
rd.initData = function() {
	rdUtils.loadDataFromClient(function(data) {
		rd.members = [];
		
		//convert bYear string (in case it string) to int
		data.allMembers.forEach(function(member) {member.bYear = parseInt(member.bYear)});

		rd.members = data;
	});
};

/** Main rower diviner function */ 
rd.main = function() {
	rd.initData();

	var filteredMembersBySide = rd.filterBySide(rd.members.allMembers)
	var dividenMembers = rd.divideBySide(filteredMembersBySide);
	var boats = rd.fillBoatsWithMembers(dividenMembers);

	return boats;
}

/** Align members by side. 
  * @param members rd.members.allMembers structure alike array
  * @return object with divined members into right, left and both arrays */
rd.filterBySide = function(members) {
	var me = this;

	var filteredMembersBySide = {
		right : [],
		left : [],
		both : [],
	};

	members.forEach(function(member) {
		if (member.side === RIGHT) {
			filteredMembersBySide.right.push(member);
		}
		else if (member.side === LEFT) {
			filteredMembersBySide.left.push(member);
		}
		else if (member.side === BOTH) {
			filteredMembersBySide.both.push(member);
		}
	});

	return filteredMembersBySide;
};

/** Divide members by side
  * @param members rd.filterBySide.filteredMembersBySide structure alike array
  * @return object with divined members into right and left arrays */
rd.divideBySide = function(members) {
	var dividenMembers = {
		left : [],
		right : [],
	};

	// make left/right members copy
	dividenMembers.left = members.left.slice(0);
	dividenMembers.right = members.right.slice(0);

	var totalUniversal = members.both.length;
	for (var i = 0; i < totalUniversal; i++) {
		var isMoreOnLeft = (dividenMembers.left.length - dividenMembers.right.length) > 0 ? true : false;
		var womensOnRight = rd.getTotalByGender(dividenMembers.right, WOMEN);
		var womensOnLeft = rd.getTotalByGender(dividenMembers.left, WOMEN);
		

		if (isMoreOnLeft) {
			var genderPriority = womensOnLeft < womensOnRight ? WOMEN : MAN;
			var chosenMember = rd.getRandomUniversal(members.both, genderPriority);
			dividenMembers.right.push(chosenMember);
		} else {
			var genderPriority = womensOnRight < womensOnLeft ? WOMEN : MAN;
			var chosenMember = rd.getRandomUniversal(members.both, genderPriority);
			dividenMembers.left.push(chosenMember);
		}
	}

	return dividenMembers;
};

rd.getTotalByGender = function(members, gender) {
	var total = 0;
	members.forEach(function(member) {
		if (member.gender === gender) {
			total++;
		}
	});

	return total;
};

/** Helper function
  * @return number of full boats */
rd.getBoatsNumber = function(members) {
	var fullLeftSideMemberGroupsNumber = Math.floor(members.left.length / 4);
	var fullRightSideMemberGroupsNumber = Math.floor(members.right.length / 4);
	var numberOfFullBoats = Math.min(fullLeftSideMemberGroupsNumber, fullRightSideMemberGroupsNumber);

	return numberOfFullBoats;
};

/** Helper function
  * @return boats object array ready for members adding */
rd.setupBoats = function(numberOfBoats) {
	var boats = [];

	for (var i = 0; i < numberOfBoats; i++) {
		var boat = {
			leftSide : [],
			rightSide : [],
			priorityGroup : null,
			priorityAgeRange : null,
			prioritySide : null
		}

		boats.push(boat);
	}

	return boats;
};

rd.priorityAgeRange = function(age) {
	var inc = 5;
	var ageRange = {
		min: age - inc,
		max: age + inc,
	}
	return ageRange;
};

/** Helper function
  * @param ready to sit members from both sides (left and rigth)
  * @return random group from ready to sit members (e.g. women) */
rd.getRandomMemberGroup = function(readyToSitMembers) {
	var groups = [];

	readyToSitMembers.forEach(function(member) {
		// include WOMENs into group if it exist across members (indexOf used to avoid dublicates)
		if (groups.indexOf(WOMEN) < 0 && member.gender === WOMEN) {
			groups.push(WOMEN);
		}
		else if (groups.indexOf(MAN) < 0 && member.gender === MAN) {
			groups.push(MAN);	
		}
	});

	groups.shuffle();
	return groups[0];
};

/** Helper function 
	@param rd.divideBySide.dividenMembers.left/right structure a like array
	@return sorted members by age in inc order (original array not effected) */
rd.sortMembersByAge = function(members) {
	// create shallow objects list copy
	var unsortedMembers = members.slice(0);

	unsortedMembers.sort(function(a, b) {
		return a.bYear - b.bYear;
	});

	return unsortedMembers;
}

/** Helper function
	@return most senior members list */
rd.getSeniorMembers = function(members, memberNumber) {
	var sortedMembersByAge = rd.sortMembersByAge(members);
	return sortMembersByAge.splice(memberNumber);
}

/** Helper function
	sort provided members array by similarity index decreasingly (higher index will be on top) */
rd.sortMembersBySimilarityIndex = function(members) {
	members.sort(function(a, b) {
		return b.similarityIndex - a.similarityIndex;
	});
}

/** Helper function
	collect all members from boats by side */
rd.getMembersByBoatSide = function(boats, side) {
	var members = [];
	boats.forEach(function(boat) {
		if (side === RIGHT) {
			boat.rightSide.forEach(function(member) {
				member.boatSide = RIGHT;
				members.push(member);
			});
		} else if (side === LEFT) {
			boat.leftSide.forEach(function(member) {
				member.boatSide = LEFT;
				members.push(member);
			});
		}
	})

	return members;
}

/** Helper function
	check if age is between (or equal) in ageRange object min/max values */
rd.inAgeRange = function(bYear, ageRange) {
	if (ageRange === null) {
		return false;
		
	}

	if (bYear >= ageRange.min && bYear <= ageRange.max) {
		return true;
		
	} else {
		return false;

	}
};

rd.calculateSimilarityIndex = function(members, container, gender, ageRange, side) {
	// calculate similarity index
	members.forEach(function(member, index) {
		var memberData = {};
		memberData.member = member;

		// add some metadata for member wraper
		memberData.similarityIndex = 0;
		memberData.priorityGroup = false;
		memberData.priorityAgeRange = false;
		memberData.prioritySide = false;
		memberData.originIndex = index;

		if (member.gender === gender) {
			//higher number for similarity index means higher importance
			memberData.similarityIndex += 4;
		} else {
			memberData.priorityGroup = true;
		}

		if (rd.inAgeRange(member.bYear, ageRange)) {
			memberData.similarityIndex += 1;	
		} else {
			memberData.priorityAgeRange = true;
		}

		if (member.side === side) {
			memberData.similarityIndex += 2;	
		} else if (member.side === BOTH) {
			memberData.prioritySide = true;
		}
		
		container.push(memberData);
	});

	return container;
}

rd.getRandomUniversal = function(members, gender) {
	if (members.length > 1) {
		members.shuffle();	
	}
	var chosenMember = null;

	for (var i = 0; i < members.length; i++) {
		if (members[i].gender === gender) {
			chosenMember = members[i];
			members.splice(i, 1);
			return chosenMember;
		}		
	}

	chosenMember = members[0];
	members.splice(0, 1);
	return chosenMember;
}

/** Helper function 
	@param boat, used to mark priorities of group or age range	
	@param ready to sit members from one side (left or rigth)
	@param gender from which member will be selected. 
	@return member which was selected to sit. Member will be removed from readyToSitMembers array */
rd.getRandomMember = function(boat, readyToSitMembers, gender, ageRange, side) {
	if (readyToSitMembers.length < 1) {
		return null;
	}

	// in sorted members array we will keep similarity index with member reference
	memberContainer = [];

	// shuffle members to be sure of randomness
	readyToSitMembers.shuffle();

	rd.calculateSimilarityIndex(readyToSitMembers, memberContainer, gender, ageRange, side);

	// sort by similarity (members with biggest similarity index users will be on top)
	rd.sortMembersBySimilarityIndex(memberContainer);

	// select first one of shuffled members and remove from ready members list 
	var chosenMember = memberContainer[0];
	readyToSitMembers.splice(chosenMember.originIndex, 1);

	// chech if need setup priorities
	if (chosenMember.priorityGroup) {
		if (gender === null) {
			gender = chosenMember.member.gender;
		}

		boat.priorityGroup = gender;
	}

	if (chosenMember.priorityAgeRange) {
		if (ageRange === null) {
			ageRange = rd.priorityAgeRange(chosenMember.member.bYear);
		}
		boat.priorityAgeRange = ageRange;
	}

	if (chosenMember.prioritySide) {
		if (side === null) {
			side = chosenMember.member.side;
		}
		boat.prioritySide = side;
	}

	return chosenMember;
};

/** Assign user to boat seats 
  * @param rd.divideBySide.dividenMembers object
  * @return boats object */
rd.fillBoatsWithMembers = function(members) {
	// prepare members
	var readyForLeftSide = [];
	var readyForRightSide = [];
	readyForLeftSide = members.left.slice(0);
	readyForRightSide = members.right.slice(0);
	var chosedSideMembers = null;
	var chosedBoatSide = null;

	// prepare boats
	var boats = rd.setupBoats(rd.getBoatsNumber(members));

	// flag which will determine on which side need add member
	var onLeft = true;



	for (var i = 0; i < rd.boat_seats; i++) {
		var readyToSitMembers = readyForLeftSide.concat(readyForRightSide);
		var group = rd.getRandomMemberGroup(readyToSitMembers);
		var ageRange = null;
		var side = null;

		// assign one member for each boat
		boats.forEach(function(boat) {
			var boatGroup = group;
			var boatSide = side;
			var boatAgeRange = ageRange;

			// check if boat have priotity for some group
			if (boat.priorityGroup != null) {
				boatGroup = boat.priorityGroup;
				boat.priorityGroup = null;
			}

			if (boat.priorityAgeRange != null) {
				boatAgeRange = boat.priorityAgeRange;
				boat.priorityAgeRange = null;
			}

			if (boat.prioritySide != null) {
				boatSide = boat.prioritySide;
				boat.prioritySide = null;
			}

			if (onLeft) {
				chosedSideMembers = readyForLeftSide;
				chosedBoatSide = boat.leftSide;
			} else {
				chosedSideMembers = readyForRightSide;
				chosedBoatSide = boat.rightSide;
			}

			var wrappedMember = rd.getRandomMember(boat, chosedSideMembers, boatGroup, boatAgeRange, boatSide);

			chosedBoatSide.push(wrappedMember.member);

			if (side === null) {
				side = boatSide != null? boatSide : boat.prioritySide;
			}
			if (ageRange === null) {
				ageRange = boatAgeRange != null? boatAgeRange : boat.priorityAgeRange;
			}
 		});

		// swich sides
 		onLeft = onLeft ? false : true;
	}

	rd.unluckyMembers = readyForLeftSide.concat(readyForRightSide);
	return boats;
};
