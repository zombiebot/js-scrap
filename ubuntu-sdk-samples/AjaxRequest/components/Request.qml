import QtQuick 2.0

Item {
    function sendRequest(isPost, url, params, callback) {
        var action = isPost ? "post" : "get"

        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                callback(request.responseText);
            }
        }

        request.open(action, url);
        request.setRequestHeader("Content-Encoding", "UTF-8");

        if (action === "get") {
            request.send();
        } else {
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            request.send(params);
        }
    }
}
