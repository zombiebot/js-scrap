import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1 as ListItem
import Ubuntu.Layouts 0.1
import 'Layouts'

MainView {
    id: root
    width: units.gu(100)
    height: units.gu(75)

    Layouts {
        id: layouts
        layouts: [
            ConditionalLayout {
                when: root.width > units.gu(50)
                name: "bigScreen"

                LayoutBigScreen {
                    id: bigScreenLayoutId
                }
             },

            ConditionalLayout {
                when: root.width < units.gu(50)
                name: "smallScreenLayout"

                LayoutSmallScreen {
                    id: smallScreenLayoutId
                }
             }

        ]
    }
}






