import QtQuick 2.0
import Ubuntu.Components 0.1

PageStack {
    id: bigPagePageStack
    Component.onCompleted: push(initPage)

    Page {
        id: initPage
        title: i18n.tr("Page for big screen")
        visible: false

        Label {
            text: "big screen content..."
        }
    }
}


