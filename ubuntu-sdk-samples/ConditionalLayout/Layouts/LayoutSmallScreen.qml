import QtQuick 2.0
import Ubuntu.Components 0.1

PageStack {
    id: smallPagePageStack
    Component.onCompleted: push(initPage)

    Page {
        id: initPage
        title: i18n.tr("Page for small screen")
        visible: false

        Label {
            text: "small screen content..."
        }
    }
}
