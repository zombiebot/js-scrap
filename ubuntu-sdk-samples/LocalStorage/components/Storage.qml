import QtQuick.LocalStorage 2.0
import QtQuick 2.0

// Documentation:
// http://qt-project.org/doc/qt-5.1/qtquick/qmlmodule-qtquick-localstorage2-qtquick-localstorage-2.html

// Sqlite DB location
// http://www.qtcentre.org/threads/54310-Where-is-located-the-SQLITE-Database-file-used-within-QtQuick-2-0-Applications
// Qt Quick Application:
// /home/<USER>/.local/share/<ProjectName>/QML/OfflineStorage/Databases/<SOME_HASH_CODE>.sqlite
// Qt Quick UI using qmlscene:
// /home/<USER>/.local/share/Qt Project/QtQmlViewer/QML/OfflineStorage/Databases/<SOME_HASH_CODE>.sqlite

Item {
    function saveText(text) {
        var db = openDB();

        db.transaction(function(tx) {
            // Create the database table if it doesn't already exist
            tx.executeSql('CREATE TABLE IF NOT EXISTS Texts(Text TEXT)');

            // Add (another) text row
            tx.executeSql('INSERT INTO Texts VALUES(?)', [text]);
        });
    }

    function getText() {
        var db = openDB();
        var texts = ''

        db.transaction(function(tx) {
            // Create the database table if it doesn't already exist
            tx.executeSql('CREATE TABLE IF NOT EXISTS Texts(Text TEXT)');

            // Show all added texts
            var result = tx.executeSql('SELECT * FROM Texts');
            var r = "";
            for (var i = 0; i < result.rows.length; i++) {
                r += result.rows.item(i).Text + "\n"
            }
            texts = r;
        });

        return texts;

    }

    function removeByText(text) {
        var db = openDB();

        db.transaction(function(tx) {
            // Create the database table if it doesn't already exist
            tx.executeSql('CREATE TABLE IF NOT EXISTS Texts(Text TEXT)');

            // Delete rows by text
            tx.executeSql('DELETE FROM Texts WHERE Text = ?', [text]);
        });
    }

    function removeAllData() {
        var db = openDB();

        db.transaction(function(tx) {
            // Drop database table
            tx.executeSql('DROP TABLE Texts');
        });
    }

    function openDB() {
        // myDB - identifier
        // 1.0 - version
        // The Example QML SQL! - description
        // 1000000 - estimated_size
        var db = LocalStorage.openDatabaseSync("myDB", "1.0", "The Example QML SQL!", 1000000);

        return db;
    }
}
