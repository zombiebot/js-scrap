$(document).ready(function() {
    if (typeof localStorage["trollforum"] === 'undefined') {
            localStorage["trollforum"] = '0';
            $('input[name=trollforum][value=0]').click();
    }else{
        if (localStorage["trollforum"] == '0'){
            $('input[name=trollforum][value=0]').click();
        }else{
            $('input[name=trollforum][value=1]').click();
        }
    }
    $('input[name=trollforum]').change(function() {
        localStorage["trollforum"] = $('input[name=trollforum]:checked').val();
    });
});
